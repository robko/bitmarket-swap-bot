#include <limits.h>
#include <gtest/gtest.h>
#include "../c_simple_circular_buffer.hpp"

TEST(circular_buffer, general_test) {

	typedef std::tuple<size_t, double, bool> _data_tuple;
	simple_circular_buffer<_data_tuple, 3> test_buff;

	int iteration=0;
	while (iteration <= 5) {
		size_t size_value = 0; double double_value = 0;
		bool flag;

		size_value = iteration; double_value = iteration*1.1; flag = iteration%2;
		test_buff.push_back( _data_tuple(size_value, double_value, flag) );

		iteration++;
	}

	// test at() function
	EXPECT_TRUE( std::get<0>(test_buff.at(0)) == 5
			  && std::get<2>(test_buff.at(0)) == true);
	EXPECT_NEAR( std::get<1>(test_buff.at(0)), 5.5, 0.00001);

	EXPECT_TRUE( std::get<0>(test_buff.at(1)) == 4
			  && std::get<2>(test_buff.at(1)) == false);
	EXPECT_NEAR( std::get<1>(test_buff.at(1)), 4.4, 0.00001);

	EXPECT_TRUE( std::get<0>(test_buff.at(2)) == 3
			  && std::get<2>(test_buff.at(2)) == true);
	EXPECT_NEAR( std::get<1>(test_buff.at(2)),3.3,0.00001);

	// test front() and back()
	EXPECT_TRUE( std::get<0>(test_buff.at(0)) == std::get<0>(test_buff.front())
			  && std::get<1>(test_buff.at(0)) == std::get<1>(test_buff.front())
			  && std::get<2>(test_buff.at(0)) == std::get<2>(test_buff.front()) );
	EXPECT_TRUE( std::get<0>(test_buff.at(2)) == std::get<0>(test_buff.back())
			  && std::get<1>(test_buff.at(2)) == std::get<1>(test_buff.back())
			  && std::get<2>(test_buff.at(2)) == std::get<2>(test_buff.back()) );

	// test for each loop
	std::array<_data_tuple, 3> correct_values = { 	_data_tuple(5, 5.5, true),
													_data_tuple(4, 4.4, false),
													_data_tuple(3, 3.3, true) };
	int num = 0;
	for(auto &record : test_buff) {
		EXPECT_TRUE( std::get<0>(record) == std::get<0>(correct_values.at(num))
				  && std::get<2>(record) == std::get<2>(correct_values.at(num)) );
		EXPECT_NEAR( std::get<1>(record), std::get<1>(correct_values.at(num)), 0.00001 );
		num++;
	}
}
