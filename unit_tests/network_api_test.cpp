#include <limits.h>
#include <gtest/gtest.h>
#include "../c_network_api.hpp"

TEST(network_api_test, hmac_positive) {
	const std::string data[10] =
	 	{"Kp7lXUNir2Fgx78foPOsmLNmYXlr9Fcu9YEj57Jn",
		"rO8ZtttYx7SbKWUxiFiS73jPiS1m4ARybhYCYI2",
		"wCaL9hqrSCXvyzusE7zwLTqO4OJsnkKPDHaxo7",
		"6Zz72nxEgU77VxwHQwqN18pnrkKSPlFvwWDlX",
		"5fyRzwEAsjPydhUnqXYZ7UOdAm7zugsrORXc",
		"7s1nNHPaMQ91XOJqr6yCWTd1P9u81cXR6Ao",
		"xTEZTO8Ip1ydLY6NBQi1EbYSlcSfdnHszM",
		"XpN7obcGtInhRzw73xDWJVCPtypo034DO",
		"JSSbrFz0vfC3OTdvzCja6h6ZMTV6hrua",
		"B8lWZPWAWL6krkHKzxAMEM1ejrIkpWE"};

	const std::string key[10] =
		{"i8BeWLVTIYSJC12rQTEIH",
		"9HbYDHJ6gNyLrI4mF8weRw",
		"ZSAJKDaahUbFbsHh8NauiVK",
		"gdmfxjsoeKlJauqOB4YTJL8G",
		"Rw14FcWdQzoyK2eXaaTCbY1kM",
		"KV9AE5ZeVo6FovGZnDj8n18Gyc",
		"uCw8QmFrI9V5ATk5vGLGzXXjCeD",
		"VBxRnzBxotvIEu2GVFQpjuBrXU8G",
		"YqAJRy1v5hU1aZ34trPMAz9vvMMJp",
		"exkCK2LWnXiH4BBDIK2xWQN3Fg0cjJ"};

	// expected values from http://www.freeformatter.com/hmac-generator.html
	c_network_api network_api;
	EXPECT_TRUE(network_api.hmac_sha512_sign(data[0],  key[0]) == "5a825099fcd64688f671fd922a87cda268a59386ce221957d2699a599de2f2bead8d72bef3fd78975e7ab019b136c5c168f5a1c0490b01c1690ba7e57f20b6ab");
	EXPECT_TRUE(network_api.hmac_sha512_sign(data[1],  key[1]) == "61e4986b239b9ac836a274641c630f90e649cb995bcd0724893d308435f159a0de1b8a293dff895d52f12fd33c263f62925108585578864e9554a232d5d801dd");
	EXPECT_TRUE(network_api.hmac_sha512_sign(data[2],  key[2]) == "9e00f7dd2401ab212b7ae0caf21f0d977ada6a5a64a6ff28ab4dc2445de4767f5f758d8a6ab962aab3c07b26e3ffe5ec9a4a1329088f59a8c0e11a4cbe0dba9c");
	EXPECT_TRUE(network_api.hmac_sha512_sign(data[3],  key[3]) == "56ad9ab975982f23aeb2e51cb838da65ed1ef9ec8fff03523f3d418021fc225c14d1188c45cb95d25aa402a97216c163b7bb01cc4988164fa92d46a26fee534c");
	EXPECT_TRUE(network_api.hmac_sha512_sign(data[4],  key[4]) == "eb288679c400961ef23ffe075bdf1decf4c4db499a385145ba7b379aa62f4ef58e4d3b30cd6e5e0f683e4a45b1c8f105eedc58c1ccdf986869c9640d27647c7f");
	EXPECT_TRUE(network_api.hmac_sha512_sign(data[5],  key[5]) == "34d90a458ad807b5d0c62cc3c0889977222ea4a6f747674e3c3143a2835d6a5e3a36ae764f529debef163cd24758c850e7d72e8b34a048c9f8da387bf70b1d35");
	EXPECT_TRUE(network_api.hmac_sha512_sign(data[6],  key[6]) == "6b881e149968e03d685e2666c2225195f75ce10d85d0c15d7a11624416598a89a1c1766c81a9334efba5ded21cf7c4caad1fedfadec6c21c7ac8c40874b7fbaa");
	EXPECT_TRUE(network_api.hmac_sha512_sign(data[7],  key[7]) == "79c6ab58f79d3d172def33eee1457a42ac5e28e725aad3b7ade43d7604ced6252517fc6187b35a7996ed36c2573ac600e3e0a3e78686f45fad6d755e06abc7ae");
	EXPECT_TRUE(network_api.hmac_sha512_sign(data[8],  key[8]) == "cd2d77db70141b129575b24b2d14e809052e1bc70c48183b3730743830c2d344df61cef54e294278c60fbb3f1d6770e37d941efcb9a6bab74a366272c867cbca");
	EXPECT_TRUE(network_api.hmac_sha512_sign(data[9],  key[9]) == "48763e0f2e6ade90e540d2c66fd065adf5e9b4851e0980960d18e6e9e778443c3513d85bb4e13daea2db43500770b682a34be85693dc3f99e963f52419393313");
}

TEST(network_api_test, hmac_exceptions) {
	c_network_api network_api;
	EXPECT_THROW(network_api.hmac_sha512_sign("asdasaf",  ""),  std::invalid_argument);
	EXPECT_THROW(network_api.hmac_sha512_sign("",  "igirshgh"),  std::invalid_argument);
	EXPECT_THROW(network_api.hmac_sha512_sign("",  ""),  std::invalid_argument);
	EXPECT_NO_THROW(network_api.hmac_sha512_sign("asdasaf",  "asdasd"));
}

TEST(network_api_test, str_to_bitstr_convert) {

	c_network_api network_api;
	EXPECT_EQ(network_api.str_to_bitstr(std::string(1,'\0')), "00000000");
	EXPECT_EQ(network_api.str_to_bitstr("0"),  "00110000");
	EXPECT_EQ(network_api.str_to_bitstr("dc"), "0110010001100011");
	EXPECT_EQ(network_api.str_to_bitstr("ret"), "011100100110010101110100");
}
