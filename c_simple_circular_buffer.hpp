#ifndef SIMPLE_CIRCULAR_BUFFER_HPP
#define SIMPLE_CIRCULAR_BUFFER_HPP

#include <iostream>
#include <sstream>
#include <array>

#include <thread>
#include <chrono>

///////////// print tuple

template <size_t index, typename ... args_t>
typename std::enable_if_t<index >= std::tuple_size<std::tuple<args_t...>>::value>
/* void */ print_impl(std::stringstream &ss, const std::tuple<args_t...> &tup) {
	static_cast<void>(tup);	// for warning: unused parameter
	ss << "";
}

template <size_t index, typename ... args_t>
typename std::enable_if_t<index < std::tuple_size<std::tuple<args_t...>>::value>
/* void */ print_impl(std::stringstream &ss, const std::tuple<args_t...> &tup) {
	ss << std::get<index>(tup) << '\t';
	print_impl<index + 1, args_t ...>(ss, tup);
}

template <typename ... args_t>
std::string print(const std::tuple<args_t...> &tup) {
	std::stringstream ss;
	print_impl<0>(ss, tup);
	return ss.str();
}

///////////// simple_circulat_buffer

template <typename T, size_t N>
struct simple_circular_buffer final {

	simple_circular_buffer() : m_push_index(0) { }

	size_t size() { return N; }
	void set_end_index() {
		m_push_index = N;
	}

	template <class... Args>
	void emplace_back(Args&& ... args) {
		m_container.at(m_push_index) = T(std::forward<T>(args)...);
		increment_index();
	}
	void push_back(const T &obj) {
		m_container.at(m_push_index) = obj;
		increment_index();
	}

	void push_back(T &&obj) {
		m_container.at(m_push_index) = std::move(obj);
		increment_index();
	}
	
	const T& front() const {
		if (front_index() == N)
			throw std::out_of_range("Trying to get value of end index");
		else
			return m_container.at(front_index());
	}
	T& front() {
		if (front_index() == N)
			throw std::out_of_range("Trying to get value of end index");
		else
			return m_container.at(front_index());
	}
	const T& back() const { return m_container.at(back_index()); }
	T& back() { return m_container.at(back_index()); }

	const T& end_value() const { return m_end_value; }
	T& end_value() { return m_end_value; }

	/// index in m_container array of front object
	size_t front_index() const {
		if (m_push_index == N)
			return N; // end index!
		else if (m_push_index <= 0)
			return (N-1);
		else
			return (m_push_index - 1);
	}
	/// index in m_container array of back object
	size_t back_index() const {
		return m_push_index;
	}

	void increment_index() noexcept {
		if (m_push_index == N)
			return;                                                 // do nothing, can not increment more, this is end index
		else if (m_push_index == N-1)
			m_push_index = 0;
		else
			m_push_index++;
	}
	void decrement_index() noexcept {
		if (m_push_index <= 0)
			m_push_index = (N-1);
		else
			m_push_index--;
	}

	// this member getting always proper object - modulo N
	T at(size_t index) const {
		int64_t distance = static_cast<int64_t>(front_index()) - static_cast<int64_t>(index);
		if(distance >= 0)
			return m_container.at(distance);
		else
			return m_container.at(N+distance);
	}
private:
	std::array<T, N> m_container;
	size_t m_push_index;
	T m_end_value; // rubbish value, only for determine end iterator

/////////////// simple_circular_buffer::iterator
public:
	using base_iterator = std::iterator<
						std::input_iterator_tag,	// iterator_category
						T,							// value_type
						long,						// difference_type
						T*,							// pointer
						T&							// reference
		>;

	class iterator: public base_iterator {
	public:
		using typename base_iterator::reference;
		using typename base_iterator::pointer;

		explicit iterator(simple_circular_buffer &buffer) :
			increment_counter(0),
			is_end(false),
			m_buffer_copy(buffer)
		{ }
		/// prefix
		/// decrement index to get good order, because elements in buffer are added in increment order
		iterator& operator++() {
			if (increment_counter < (N-1)) {
				m_buffer_copy.decrement_index();
				increment_counter++;
				return *this;
			} else {
				is_end = true;
				m_buffer_copy.set_end_index();
				return *this;
			}
		}
		/// postfix
		iterator operator++(int) { iterator retval = *this; ++(*this); return retval; }
		bool operator==(iterator other) const {
			// dbg
			//std::this_thread::sleep_for(std::chrono::seconds(1));
			//std::cout 	<< "this:  " << print(this->get_value()) << ", " << this->m_buffer_copy.front_index() << "\n"
			//			<< "other: " << print(other.get_value()) << ", " << other.m_buffer_copy.front_index() << "\n";

			return ( this->get_value() == other.get_value()
					 && this->m_buffer_copy.front_index() == other.m_buffer_copy.front_index());
		}
		bool operator!=(iterator other) const { return !(*this == other); }
		pointer operator->() const { return &get_value(); }
		reference operator*() const { return get_value(); }
		const reference operator*() { return get_value(); }
	private:
		size_t increment_counter;
		bool is_end;
		const T& get_value() const 	{ return is_end ? m_buffer_copy.end_value() : m_buffer_copy.front(); }
		T& get_value() 				{ return is_end ? m_buffer_copy.end_value() : m_buffer_copy.front(); }
		simple_circular_buffer m_buffer_copy;
	};
	iterator begin() {
		return iterator(*this);
	}
	const iterator begin() const {
		return iterator(*this);
	}
	iterator end() {
		iterator it = iterator(*this);
		for (size_t i = 0; i < N; ++i, ++it);
		return it;
	}
	const iterator end() const {
		iterator it = iterator(*this);
		for (size_t i = 0; i < N; ++i, ++it);
		return it;
	}
};

#endif // SIMPLE_CIRCULAR_BUFFER_HPP
