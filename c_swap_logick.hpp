#ifndef C_SWAP_LOGICK_H
#define C_SWAP_LOGICK_H

#include "c_network_api.hpp"

class c_swap_logick final {
	public:
		void start_loop();                                        ///< blocks function
		void start_simple_adpt();                                  ///< loop with simple linear adaptation
	private:
		c_network_api m_network_api;
};

#endif // C_SWAP_LOGICK_H
