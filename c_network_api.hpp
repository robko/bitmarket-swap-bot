#ifndef C_NETWORK_API_H
#define C_NETWORK_API_H

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <curl/curl.h>
#include <gtest/gtest_prod.h>
#include <string>
#include <tuple>

class c_curl_ptr {
	public:
		c_curl_ptr();
		~c_curl_ptr();
		CURL *get_raw_ptr() const;
	private:
		CURL *m_ptr;
};

class c_network_api final {
	public:
		c_network_api();
		double get_current_swap_cutoff(); ///< @returns current swap cutoff
		// transactions API
		double get_free_btc_amount();
		/**
		 * @returns swap ID, BTC amount and swap rate of first swap contract (assumption: always exists only one swap contract)
		 */
		std::tuple<size_t, double, double> get_swap_info();
		void close_swap(size_t id);
		void open_swap(double amount, double rate);
		
	private:
		std::string m_private_key;
		std::string m_public_key;
		
		std::string wget_https (const std::string &address); ///< @throw std::runtime_error
		static size_t write_cb (void *ptr, size_t size, size_t nmemb, std::string *str); ///< callback for curl

		template<typename T>
		T get_value_from_json(const std::string &json_data, const std::string &path_to_value);
		/**
		 * sends signed post query to server
		 */
		std::string send_api_request(const std::string &post_query);
		/**
		 * @returns data signed by key
		 */
		std::string hmac_sha512_sign (const std::string &data, const std::string &key);

		std::string str_to_bitstr (const std::string &input);
		std::string str_to_hexstr (const std::string &input);
		unsigned long int get_unix_timestamp (); // only for linux

		// TESTS
		FRIEND_TEST(network_api_test, hmac_positive);
		FRIEND_TEST(network_api_test, hmac_exceptions);
		FRIEND_TEST(network_api_test, str_to_bitstr_convert);

};

template<typename T>
T c_network_api::get_value_from_json(const std::string &json_data, const std::string &path_to_value) {
	namespace pt = boost::property_tree;
	std::istringstream iss(json_data);
	pt::ptree ptree;
	pt::read_json(iss, ptree);
	return ptree.get<T>(path_to_value);
	
}


#endif // C_NETWORK_API_H
// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 
