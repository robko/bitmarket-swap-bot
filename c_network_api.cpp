#include "c_network_api.hpp"

#include <bitset>
#include <ctime>
#include <fstream>
#include <openssl/hmac.h>
#include <sstream>
#include <stdexcept>

c_curl_ptr::c_curl_ptr() :
	m_ptr(nullptr)
{
	CURLcode res = curl_global_init(CURL_GLOBAL_DEFAULT);
	if(res != CURLE_OK) {
		throw std::runtime_error("curl_global_init error");
	}
	m_ptr = curl_easy_init();
	if (m_ptr == nullptr) {
		throw std::runtime_error("CURL init error");
	}
}

c_curl_ptr::~c_curl_ptr() {
	curl_easy_cleanup(m_ptr);
	curl_global_cleanup();
}

CURL *c_curl_ptr::get_raw_ptr() const {
	return m_ptr;
}


/************************************************************************/

namespace pt = boost::property_tree;
c_network_api::c_network_api() {
	// load keys
	try {
		std::ifstream input_file;
		// throw exception if file not exist does not exist
		input_file.exceptions(std::ifstream::failbit);

		input_file.open("secret.key");
		input_file >> m_private_key;
		input_file.close();
		input_file.open("public.key");
		input_file >> m_public_key;
		input_file.close();
	} catch (const std::exception &er) {
		std::string msg("Fail to load secret.key or public.key: ");
		msg += er.what();
		throw std::runtime_error(msg);
	}
}

double c_network_api::get_current_swap_cutoff() {
	const std::string raw_data = wget_https("https://www.bitmarket.pl/json/swapBTC/swap.json");
	return get_value_from_json<double>(raw_data, "cutoff");
}

double c_network_api::get_free_btc_amount() {
	const std::string json_data = send_api_request("method=info");
	return get_value_from_json<double>(json_data, "data.balances.available.BTC");
}

std::tuple<size_t, double, double> c_network_api::get_swap_info() {
	const std::string json_data = send_api_request("method=swapList&currency=BTC");
	std::istringstream iss(json_data);
	pt::ptree ptree;
	pt::read_json(iss, ptree);
	if (ptree.get_child("data").empty()) throw std::runtime_error("No open swap");
	auto swap = ptree.get_child("data").front();
	size_t id = swap.second.get<size_t>("id");
	double amount = swap.second.get<double>("amount");
	double rate = swap.second.get<double>("rate");
	return std::make_tuple(id, amount,  rate);
}

void c_network_api::close_swap(size_t id) {
	send_api_request("method=swapClose&id=" + std::to_string(id) + "&currency=BTC");
}

void c_network_api::open_swap(double amount, double rate) {
	send_api_request("method=swapOpen&currency=BTC&amount=" + std::to_string(amount) + "&rate=" + std::to_string(rate));
}


std::string c_network_api::wget_https(const std::string& address) {
	c_curl_ptr curl_ptr;
	curl_easy_setopt(curl_ptr.get_raw_ptr(), CURLOPT_URL, address.c_str());
	curl_easy_setopt(curl_ptr.get_raw_ptr(), CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(curl_ptr.get_raw_ptr(), CURLOPT_SSL_VERIFYHOST, 0L);
	std::string return_str;
	curl_easy_setopt(curl_ptr.get_raw_ptr(), CURLOPT_WRITEDATA, &return_str);
	curl_easy_setopt(curl_ptr.get_raw_ptr(), CURLOPT_WRITEFUNCTION, &c_network_api::write_cb);
	CURLcode ret_code;
	ret_code = curl_easy_perform(curl_ptr.get_raw_ptr());
	if(ret_code != CURLE_OK)
		throw std::runtime_error(std::string("curl_easy_perform() failed: ") + curl_easy_strerror(ret_code));
	return return_str;
}

size_t c_network_api::write_cb(void *ptr, size_t size, size_t nmemb, std::string *str) {
	const size_t data_size = size * nmemb; // size of received data
	if (data_size == 0) return data_size;
	str->append(static_cast<const char *>(ptr), data_size);
	return data_size;
}

std::string c_network_api::send_api_request(const std::string &post_query) {
	c_curl_ptr curl_ptr;
	curl_easy_setopt(curl_ptr.get_raw_ptr(), CURLOPT_URL, "https://www.bitmarket.pl/api2/");
	curl_easy_setopt(curl_ptr.get_raw_ptr(), CURLOPT_POST, 1L);
	const std::string post_query_complete = post_query + "&tonce=" + std::to_string(get_unix_timestamp());
	const std::string sign = hmac_sha512_sign(post_query_complete,  m_private_key);
	const std::string api_key = std::string("API-Key: ") + m_public_key;
	const std::string api_hash = "API-Hash: " + sign;
	struct curl_slist *list = nullptr;
	list = curl_slist_append(list, api_key.c_str());
	list = curl_slist_append(list, api_hash.c_str());
	curl_easy_setopt(curl_ptr.get_raw_ptr(), CURLOPT_HTTPHEADER, list);
	curl_easy_setopt(curl_ptr.get_raw_ptr(), CURLOPT_POSTFIELDSIZE,  post_query_complete.size());
	curl_easy_setopt(curl_ptr.get_raw_ptr(), CURLOPT_POSTFIELDS, post_query_complete.data());
	std::string return_str;
	curl_easy_setopt(curl_ptr.get_raw_ptr(), CURLOPT_WRITEDATA, &return_str);
	curl_easy_setopt(curl_ptr.get_raw_ptr(), CURLOPT_WRITEFUNCTION, &c_network_api::write_cb);
	
	CURLcode ret_code = curl_easy_perform(curl_ptr.get_raw_ptr());
	if(ret_code != CURLE_OK) {
		curl_slist_free_all(list);
		throw std::runtime_error(std::string("curl_easy_perform() failed: ") + curl_easy_strerror(ret_code));
	}
	curl_slist_free_all(list);
	return return_str;
}

std::string c_network_api::hmac_sha512_sign(const std::string& data, const std::string& key) {
	if (data.empty()) throw std::invalid_argument("data is empty");
	if (key.empty()) throw std::invalid_argument("key is empty");
 	constexpr size_t return_size = 64;
	std::string ret(return_size,'\0');
	ret.resize(return_size);
	
	HMAC_CTX ctx;
	HMAC_CTX_init(&ctx);
	HMAC_Init_ex(&ctx, key.data(), key.size(), EVP_sha512(), nullptr);
	HMAC_Update(&ctx, reinterpret_cast<const unsigned char*>(data.data()), data.size());
	unsigned int len = 0;
	HMAC_Final(&ctx, reinterpret_cast<unsigned char *>(&ret[0]), &len);
	HMAC_CTX_cleanup(&ctx);
	assert(ret.size() ==  return_size);
	
	ret = str_to_hexstr(ret);
	assert (ret.size() == return_size * 2);
	
	return ret;
}

std::string c_network_api::str_to_bitstr(const std::string &input) {
	std::string bitstr;
	for (auto ch : input) {
		std::string revers_str;
		for (int i = 0; i < 8; ++i) {
			if (ch % 2) {
				revers_str += "1";
			} else {
				revers_str += "0";
			}
			ch >>= 1;
		}
		std::reverse(revers_str.begin(),revers_str.end());
		bitstr += revers_str;
	}

	assert (bitstr.size() == input.size()*8);
	return bitstr;
}
// http://stackoverflow.com/questions/3381614/c-convert-string-to-hexadecimal-and-vice-versa
std::string c_network_api::str_to_hexstr(const std::string &input) {
	static const char* const lut = "0123456789abcdef";
	size_t len = input.length();
	std::string output;
	output.reserve(2 * len);
	for (size_t i = 0; i < len; ++i) {
		const unsigned char c = input[i];
		output.push_back(lut[c >> 4]);
		output.push_back(lut[c & 15]);
	}
	return output;
}

long unsigned int c_network_api::get_unix_timestamp() {
	struct timeval tv;
	gettimeofday(&tv, nullptr);
	return tv.tv_sec;
}


