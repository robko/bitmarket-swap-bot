#include "c_swap_logick.hpp"

int main(int argc, const char* argv[]) {
	try {
		c_swap_logick logick;

		if ( argc == 2 && (std::string(argv[1]) == "--with-adoption") ) {
			std::cout << "using loop with simple adoption\n";
			logick.start_simple_adpt();
		} else {
			std::cout << "using default start loop\n";
			logick.start_loop();
		}

	} catch (std::exception &er) {
		std::cerr << "Something goes wrong, catch in main:\n"
				  << er.what() << "\nExiting...\n";
		return 1;
	}
	return 0;
}
// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 
