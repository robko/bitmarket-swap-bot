#include "c_swap_logick.hpp"
#include "c_simple_circular_buffer.hpp"
#include <chrono>
#include <thread>

void c_swap_logick::start_loop() {
	while (true) {
		double cutoff = m_network_api.get_current_swap_cutoff();
		std::cout << "current cutoff " <<  cutoff << "\n";
		size_t id = 0;
		double swap_rate = 0;
		try {
			std::tie(id, std::ignore, swap_rate) = m_network_api.get_swap_info();
		} catch (const std::exception &e) {
			std::cout << "catch exception " <<  e.what() <<  std::endl;
		}                                          // no active swap
		if ( (swap_rate < (cutoff * 0.9)) || (swap_rate > (cutoff * 0.99)) ) {
			if (id !=  0)                                            // found open swap
				m_network_api.close_swap(id);
			double free_btc_amount = m_network_api.get_free_btc_amount() - 0.000001;// corrected rounding error
			std::cout << "free btc amount " << free_btc_amount << "\n";
			std::cout << "new swap, " <<  cutoff * 0.95 << "%\n";
			m_network_api.open_swap(free_btc_amount, cutoff - 0.1);

		}
		std::this_thread::sleep_for(std::chrono::seconds(20));
	}
}

/////////////////////////////                            //////////////////////////////

std::tm get_localtime (const std::string & locale_code) {

	std::time_t t = std::time(nullptr);
	std::tm tm = *std::localtime(&t);
	std::cout.imbue(std::locale(locale_code));

	return tm;
}

std::string get_formatted_time () {

	std::tm tm = get_localtime ("en_US.utf8");
	std::stringstream ss;
	ss << std::put_time(&tm, "%T");

	return ss.str();
}

/////////////////////////////                            //////////////////////////////

typedef std::tuple<std::string, size_t, double, double, double, double, bool, std::string> _data_tuple;

double predict_bet(c_network_api &m_network_api, const _data_tuple &data_snapchat, const size_t distance) {
	double cutoff_before = m_network_api.get_current_swap_cutoff();
	double safety = 0.001 * cutoff_before; // 0.1% under real cutoff

	double change_after_distance = cutoff_before - std::get<4>(data_snapchat);
	if (change_after_distance >= 0.)
		return cutoff_before - safety;
	else
		return cutoff_before + change_after_distance/distance - safety;
}

void c_swap_logick::start_simple_adpt() {

	// guess variables
	std::chrono::seconds iteration_time = std::chrono::seconds(24);
	// variables that we are interested in
	size_t id = 0;
	double amount = 0;
	double rate = 0;
	double cutoff_after = 0;
	double bet = 0;
	bool if_success = false;
	std::string additional_info;

	constexpr size_t memory_size = 10;
	size_t distance_to_analyze = 5;
	simple_circular_buffer<_data_tuple, memory_size> volatile_memory;
	size_t iter_num = 0;
	while (true) {

		additional_info = "";
try {
		// time start
		auto start = std::chrono::steady_clock::now();

		double new_bet = predict_bet(m_network_api, volatile_memory.at(distance_to_analyze), distance_to_analyze);

		if(new_bet != bet) {
			bet = new_bet;

			try {
				std::tie(id, amount, rate) = m_network_api.get_swap_info();
				if (id != 0) {                         // found open swap
					m_network_api.close_swap(id);
				}
			} catch (const std::exception &ec) {
				additional_info += "catch exception: " +  std::string(ec.what()) + ", ";
			}

			double free_btc_amount = m_network_api.get_free_btc_amount() - 0.000001;// corrected rounding error
			m_network_api.open_swap(free_btc_amount, bet);
			additional_info += "new open: "  + std::to_string(bet) + ", ";

		}

		// time end
		auto end = std::chrono::steady_clock::now();
		auto diff_s = std::chrono::duration_cast<std::chrono::seconds>(end - start);

		cutoff_after = m_network_api.get_current_swap_cutoff();
		if (bet <= cutoff_after)
			if_success = true;
		else
			if_success = false;

		if(diff_s > iteration_time) {
			std::cout << "Overstep iteration time! deadline[" << iteration_time.count()
					  << "] > time [" << diff_s.count() << "]\n";
		} else {
			// std::cout << "Strat waiting: itetime [" << iteration_time.count() << "], diff [" <<  diff_s.count() << "]\n";
			std::this_thread::sleep_for(iteration_time - diff_s);
		}

		volatile_memory.push_back( _data_tuple(get_formatted_time(),
											   id,
											   amount,
											   rate,
											   cutoff_after,
											   bet,
											   if_success,
											   additional_info) );

		if (!(iter_num % 24))
			std::cout << "time,\t\tid,\tamnt,\t\trate,\tcutf_a,\tbet,\tsuccess\tadditional_info\n";
		std::cout << "print\n";
		std::cout << print(volatile_memory.front()) << std::endl;
		iter_num++;
} catch (const std::exception &ec) {
	std::cout << ec.what() << " - Something goes wrong in adoption loop, restarting in 60 second ...\n";
	std::this_thread::sleep_for(std::chrono::seconds(60));
}
	}
}
